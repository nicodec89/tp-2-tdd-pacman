const expect = require("chai").expect;
const should = require('should');
const assert = require('assert');

var pacmanTestDriver =  require('../src/pacmanTestDriver');

describe("GHOST Functionalities", function () {

    before(function () {
        // runs before all tests in this block
    });

    after(function () {
        // runs after all tests in this block
    });

    beforeEach(function () {
        // runs before each test in this block
    });

    afterEach(function () {
        // runs after each test in this block
    });


    describe('A KEEPER hunter ghost', function () {

        const maze = [
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0],
            [0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0],
            [0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0],
            [0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0],
            [0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        ];
        const impassableTypesTest = [0, 2];
        const options = {
            random: [pacmanTestDriver.RANDOM.TOP, pacmanTestDriver.RANDOM.BOTTOM, pacmanTestDriver.RANDOM.WEST, pacmanTestDriver.RANDOM.EAST],
            iterations: 1
        }
        const pacmanInitialPosition = {
            columnIndex: 1,
            rowIndex: 1,
            direction: pacmanTestDriver.COMPASS.WEST,
            nextMovements: []
        };
        const ghostInitialPosition = {
            columnIndex: 3,
            rowIndex: 4,
            direction: pacmanTestDriver.COMPASS.EAST,
            ghostType: pacmanTestDriver.GHOST_TYPE.KEEPER,
            keepPosition: {
                columnIndex: 7,
                rowIndex: 4
            }
        };

        it('should after 1 movement, continue in his current direction until arrive to a bifurcation', function () {

            options.iterations = 1;

            const result = pacmanTestDriver.driveCase({
                mazeConfiguration: {
                    maze: maze,
                    impassableTypesTest: impassableTypesTest
                },
                pacmanInitialPosition: pacmanInitialPosition,
                ghostInitialPosition: ghostInitialPosition,
                options: options
            });

            assert(result.ghostPosition.columnIndex == 4);
            assert(result.ghostPosition.rowIndex == 4);
        });

        it('should after 2 movements, continue in his current direction until arrive to a bifurcation', function () {

            options.iterations = 2;

            const result = pacmanTestDriver.driveCase({
                mazeConfiguration: {
                    maze: maze,
                    impassableTypesTest: impassableTypesTest
                },
                pacmanInitialPosition: pacmanInitialPosition,
                ghostInitialPosition: ghostInitialPosition,
                options: options
            });

            assert(result.ghostPosition.columnIndex == 5);
            assert(result.ghostPosition.rowIndex == 4);
        });

        it('should after 3 movements, continue NORTH in the bifurcation because at equals distance to the keep position choice using Random and choose first NORTH', function () {

            options.iterations = 3;

            const result = pacmanTestDriver.driveCase({
                mazeConfiguration: {
                    maze: maze,
                    impassableTypesTest: impassableTypesTest
                },
                pacmanInitialPosition: pacmanInitialPosition,
                ghostInitialPosition: ghostInitialPosition,
                options: options
            });

            assert(result.ghostPosition.columnIndex == 5);
            assert(result.ghostPosition.rowIndex == 3);

        });

        it('should after 4 movements, continue EAST in the bifurcation because is the shortest way to the keep position', function () {

            options.iterations = 4;

            const result = pacmanTestDriver.driveCase({
                mazeConfiguration: {
                    maze: maze,
                    impassableTypesTest: impassableTypesTest
                },
                pacmanInitialPosition: pacmanInitialPosition,
                ghostInitialPosition: ghostInitialPosition,
                options: options
            });

            assert(result.ghostPosition.columnIndex == 6);
            assert(result.ghostPosition.rowIndex == 3);

        });

        it('should after 5 movements, continue in his way until arrive to a bifurcation', function () {

            options.iterations = 5;

            const result = pacmanTestDriver.driveCase({
                mazeConfiguration: {
                    maze: maze,
                    impassableTypesTest: impassableTypesTest
                },
                pacmanInitialPosition: pacmanInitialPosition,
                ghostInitialPosition: ghostInitialPosition,
                options: options
            });

            assert(result.ghostPosition.columnIndex == 7);
            assert(result.ghostPosition.rowIndex == 3);

        });

        it('should after 6 movements, continue BOTTOM in the bifurcation because is the shortest way to the keep position', function () {

            options.iterations = 6;

            const result = pacmanTestDriver.driveCase({
                mazeConfiguration: {
                    maze: maze,
                    impassableTypesTest: impassableTypesTest
                },
                pacmanInitialPosition: pacmanInitialPosition,
                ghostInitialPosition: ghostInitialPosition,
                options: options
            });

            assert(result.ghostPosition.columnIndex == 7);
            assert(result.ghostPosition.rowIndex == 4);

        });

        it('should after 7 movements, continue in his way until arrive to a bifurcation', function () {

            options.iterations = 7;

            const result = pacmanTestDriver.driveCase({
                mazeConfiguration: {
                    maze: maze,
                    impassableTypesTest: impassableTypesTest
                },
                pacmanInitialPosition: pacmanInitialPosition,
                ghostInitialPosition: ghostInitialPosition,
                options: options
            });

            assert(result.ghostPosition.columnIndex == 7);
            assert(result.ghostPosition.rowIndex == 5);

        });

        it('should after 8 movements, continue WEST in the bifurcation because at equals distance to the keep position choice using Random and try second BOTTOM but no valid for this bifurcation, so try third option WEST', function () {

            options.iterations = 8;

            const result = pacmanTestDriver.driveCase({
                mazeConfiguration: {
                    maze: maze,
                    impassableTypesTest: impassableTypesTest
                },
                pacmanInitialPosition: pacmanInitialPosition,
                ghostInitialPosition: ghostInitialPosition,
                options: options
            });

            assert(result.ghostPosition.columnIndex == 6);
            assert(result.ghostPosition.rowIndex == 5);

        });
    });
});
