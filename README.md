# Tp0Pacman


**Trabajo práctico 2 - Técnicas de diseño - FIUBA - 1cuat 2019
  Pacman**

#### Este trabajo consiste . 

>Para poder lanzar el contenedor se debe buildear previamente el contenedor con:

+ `docker-compose build`

>Al terminar de descargar lo necesario para la imagen de docker, se puede lanzar la api con:
+ `docker-compose up`

>Para correr los tests lanzar con:
+ `docker-compose run app npm test`

***
 
[Repositorio](https://gitlab.com/nicodec89/tp-2-tdd-pacman)

  
**Hecho por Maximiliano Burastero, Nicolas Deciancio y Ariel Ruiz.**