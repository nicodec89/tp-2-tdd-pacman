const Game = require('./Game');

const pacmanTestDriver = {
    driveCase: function (config) {
        const game = new Game(config);
        return game.run(config.options.iterations);
    },
    COMPASS : {
        NORTH: "NORTH",
        SOUTH: "SOUTH",
        WEST: "WEST",
        EAST: "EAST"
    },
    RANDOM : {
        TOP: "TOP",
        BOTTOM: "BOTTOM",
        WEST: "WEST",
        EAST: "EAST"
    },
    GHOST_TYPE : {
        SEARCHER: "SEARCHER",
        TEMPERAMENTAL_SEARCHER: "TEMPERAMENTAL_SEARCHER",
        LAZY: "LAZY",
        KEEPER: "KEEPER"
    }
};
module.exports = pacmanTestDriver;