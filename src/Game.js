const Pacman = require('../app/src/model/Pacman');
const Ghost = require('../app/src/model/Ghost');
const Maze = require('../app/src/maze/Maze');

const COMPASS = {
    NORTH: Pacman.COMPASS.NORTH,
    SOUTH: Pacman.COMPASS.SOUTH,
    WEST: Pacman.COMPASS.WEST,
    EAST: Pacman.COMPASS.EAST
};

const GHOST_TYPE = {
    SEARCHER: Ghost.GHOST_TYPE.SEARCHER,
    TEMPERAMENTAL_SEARCHER: Ghost.GHOST_TYPE.TEMPERAMENTAL_SEARCHER,
    LAZY: Ghost.GHOST_TYPE.LAZY,
    ZONZO: Ghost.GHOST_TYPE.ZONZO,
    KEEPER: Ghost.GHOST_TYPE.KEEPER,

};

/**
 * A facade to implement Game
 */
class Game {

    constructor( config) {
        Maze.deleteInstance();
        this._maze = new Maze(config.mazeConfiguration.maze, config.mazeConfiguration.impassableTypesTest).getInstance();

        this._pacman = new Pacman(
            this._maze,
            {row: config.pacmanInitialPosition.rowIndex, col: config.pacmanInitialPosition.columnIndex},
            COMPASS[config.pacmanInitialPosition.direction]
        );

        this._ghost = new Ghost(
            this._maze.getCell(config.ghostInitialPosition.rowIndex, config.ghostInitialPosition.columnIndex),
            GHOST_TYPE[config.ghostInitialPosition.ghostType]
        );
        if (config.ghostInitialPosition.keepPosition !== undefined ) {
            let rowIndexKeeep = config.ghostInitialPosition.keepPosition.rowIndex !== undefined ? config.ghostInitialPosition.keepPosition.rowIndex : null;
            let colIndexKeeep = config.ghostInitialPosition.keepPosition.columnIndex !== undefined ? config.ghostInitialPosition.keepPosition.columnIndex: null;

            this._ghost.setKeepPosition({ rowIndex: rowIndexKeeep, columnIndex: colIndexKeeep});
        }

        this._pacman.subscribe(this._ghost);
    }

    run( iterations ) {
        for (let i = 0; i < iterations; i++) {
            this._pacman.moveAStep();
        }

        return {
            ghostPosition: {
                columnIndex: this._ghost.getPosition().col,
                rowIndex: this._ghost.getPosition().row
            }
        }
    }
}

module.exports = Game;