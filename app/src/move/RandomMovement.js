const Movement  = require('./Movement');
const Position  = require('./Position');

class RandomMovement extends Movement{

    constructor() {
        super();
    }


    moveAStep( pos, prevPos) {

        let movingTo    = pos.getRandomStep( prevPos );
        return movingTo;
    }

}

module.exports = RandomMovement;