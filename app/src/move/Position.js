const Connections = require('./Connections');

class Position {

    constructor( pos )
    {
        this._row = pos.row;
        this._col = pos.col;
        this.setConnections(new Connections( pos.connections ));
    }

    get row() {
        return this._row;
    }

    set row(value) {
        this._row = value;
    }

    get col() {
        return this._col;
    }

    set col(value) {
        this._col = value;
    }
    getConnections() {
        return this._connections;
    }

    setConnections(value) {
        this._connections = value;
    }

    getRandomArbitrary(min, max) {
        return Math.floor(Math.random() * (+max - +min)) + +min;
    }

    static areEquals( aPosition, otherPosition ) {

        let rowEq  = (aPosition.row === otherPosition.row);
        let colEq  = (aPosition.col === otherPosition.col);

        return ( rowEq && colEq);
    }

    getDifferentPosition( newPosition, prevPosition) {
        let posToMove = new Position(newPosition);
        return Position.areEquals(posToMove, prevPosition) ? this.getRandomStep(prevPosition) : posToMove;
    }

    getRandomStep( prevPosition ) {
        //get random pos
        let newPosition = this.getRandomArbitrary(0, this.getConnections().getNeighbors().length);
        //moving to new position
        let stepTo = this.getConnections().getNeighbors()[newPosition];
        stepTo = this.getDifferentPosition(stepTo, prevPosition);

        return stepTo;
    }

    transformData(){
        return {
            row: this.row,
            col: this.col,
            connections: this.getConnections().getNeighbors()
        }
    }

    distance(otherPosition) {
        return Math.abs(this.row - otherPosition.row) + Math.abs(this.col - otherPosition.col);
    }

    clonePosition() {
        return new Position({
            row: this.row,
            col: this.col,
            connections: this._connections.getNeighbors()
        });
    }

    static createAPositionWithoutConnections( pos ) {
        return new Position({
                row: pos.rowIndex,
                col: pos.columnIndex,
                connections: [],
        });
    }
}

module.exports = Position;