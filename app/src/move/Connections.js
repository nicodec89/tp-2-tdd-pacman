

class Connections {

    constructor( neighbors ) {
        this._neighbors = neighbors;
    }

    getNeighbors() {
        return this._neighbors;
    }

    setNeighbors(value) {
        this._neighbors = value;
    }

    defineNeighbors( row, col, neighbors ) {
        let left = this.setNeighborsLeft(row, col, neighbors);
        this._left = (left !== undefined) ? left : [];

        let right = this.setNeighborsRight(row, col, neighbors);
        this._right = (right !== undefined) ? right : [];

        let up = this.setNeighborsUp(row, col, neighbors);
        this._up = (up!== undefined) ? up : [];

        let down = this.setNeighborsDown(row, col, neighbors);
        this._down = (up!== undefined) ? down : [];
    }

    setNeighborsLeft(row, col, neighbors) {
        neighbors.find( (cell) => {
            return (row === cell.row && col - 1 === cell.col);
        });
    }

    setNeighborsRight(row, col, neighbors) {
        neighbors.find( (cell) => {
            return (row === cell.row && col + 1 === cell.col);
        });
    }

    setNeighborsUp(row, col, neighbors) {
        neighbors.find( (cell) => {
            return (row - 1 === cell.row && col === cell.col);
        });
    }

    setNeighborsDown(row, col, neighbors) {
        neighbors.find( (cell) => {
            return (row + 1 === cell.row && col === cell.col);
        });
    }


}

module.exports = Connections;