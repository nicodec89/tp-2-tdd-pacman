const Movement  = require('./Movement');
const Maze = require('../maze/Maze');
const Position = require('../move/Position');

class DirectedMovement extends Movement {
    constructor() {
        super();
    }


    moveAStep( pos, prevPos, posPacman) {
        let maze = Maze.getInstance();
        let cell = maze.getCell(pos.row, pos.col);
        let cellPacman = maze.getCell(posPacman.row, posPacman.col);
        let cellPrev = maze.getCell(prevPos.row, prevPos.col);
        let nextCell = maze.getNextCellOnThePath(cell, cellPacman, cellPrev);

        return new Position(nextCell);
    }
}

module.exports = DirectedMovement;