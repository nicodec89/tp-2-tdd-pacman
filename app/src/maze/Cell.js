const MazeUnit = require("./MazeUnit");

/**
 * A cell is an unit that can be walked through as a corridor.
 */
class Cell extends MazeUnit {

    /**
     * Determines if the cell is passable.
     *
     * @return {boolean} True as this is considered a passable unit.
     */
    get canGoTrough() {
        return true;
    }

    /**
     * Starts a trip from this cell around the maze to check if you can
     * return to it.
     *
     * @return The shortest path traveled.
     */
    get startTrip() {
        return this.findConnection([], this);
    }

    /**
     * Finds the target unit through all of this unit connections.
     *
     * @param target The target unit to search.
     * @return A list with the path to the target unit, empty if the unit
     * was not found.
     */
    findCell(target) {
        return this.findConnection([], target);
    }

    /**
     * Method that searches for a target unit through this unit's connections
     * recursively. It needs a path as parameter that represents the path
     * traveled trough the current unit to avoid repeating visits.
     *
     * @param path The path traveled until current position, cannot be null.
     * @param target The target unit to search for.
     * @return {*} A list that represents the shortest path to the target
     * unit, might be null.
     */
    findConnection(path, target) {
        if (this.connections && this.connections.length > 1) {
            const lastVisited = path.length > 0 && path[path.length - 1];
            let match = this.connections.filter(cell =>
                (cell.isEquals(target) || (target.isEquals(path[0]) && path.find(element => element.isEquals(cell))))
                && !(lastVisited && lastVisited.isEquals(cell)));
            if (match && match.length) {
                return [...path, this];
            } else {
                let foundCellPaths = this.connections.filter(cell =>
                        !path.find(element => element.isEquals(cell))
                        && !(lastVisited && lastVisited.isEquals(cell)))
                    .map(cell => cell.findConnection([...path, this], target))
                    .filter(resultPath => resultPath && resultPath.length)
                    .sort((a, b) => b.length - a.length);
                return foundCellPaths.length && foundCellPaths.pop();
            }
        }
    }

}

module.exports = Cell;

