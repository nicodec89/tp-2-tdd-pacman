const MazeUnit = require("./MazeUnit");

/**
 * A block is an unit that cannot be walked through like a wall or a pit in a
 * maze.
 */
class Block extends MazeUnit {

    /**
     * Determines if the cell is passable.
     *
     * @return {boolean} False as this is considered a non passable unit.
     */
    get canGoTrough() {
        return false;
    }

    /**
     * Starts a trip from this cell around the maze to check if you can
     * return to it.
     *
     * @return A list that contains only this unit as it hasn't any connection.
     */
    get startTrip() {
        return [this];
    }
}

module.exports = Block;