const unitFactory = require("./MazeUnitFactory");
const PF = require('pathfinding');

/**
 * Represents Maze.js. A Maze.js is a matrix of {@link MazeUnit mazeUnits} that
 * can be cells or blocks.
 */
class Maze {

    /**
     * Maze.js constructor with mandatory parameters. May throw an error if a
     * cell matrix is not defined.
     *
     * @param cellMatrix The matrix of the maze configuration, cannot be
     * null nor empty.
     * @param nonPassableTypes The array of types that should be consider as
     * no passable in the matrix, might be null.
     */
    constructor(cellMatrix, nonPassableTypes) {
        if (!cellMatrix || !cellMatrix.length) {
            throw Error("Cell matrix cannot be null nor empty.");
        }
        this.cells = cellMatrix.map((row, rowNum) => {
            return row.map((type, colNum) => {
                return unitFactory.createMazeUnit(rowNum, colNum, type, nonPassableTypes);
            });
        });
        this.cells.forEach((row) => {
            return row.forEach((cell) => {
                cell.addConnections(this.cells);
            });
        });
        this.createGrid(cellMatrix, nonPassableTypes);
    }

    /**
     * Retrieves all the cells that are passable.
     *
     * @returns {Uint8Array | number[]} The passable cells list, might be null.
     */
    getPassableCells() {
        let passableCells = this.cells.map(row => row.filter(cell => cell.canGoTrough));
        return passableCells && [].concat.apply([], passableCells);
    }

    /**
     * Checks that all cells in the maze are reachable with each other.
     *
     * @returns {Uint8Array | number[] | boolean} True if all the cells are
     * related.
     */
    checkCellsConnectivity() {
        let passableCells = this.cells.map(row => row.filter(cell => cell.canGoTrough));
        let passableCellsFlat = passableCells && [].concat.apply([], passableCells);
        return passableCellsFlat && passableCellsFlat.map(i =>
            passableCellsFlat.map(j => i.findCell(j))
                .reduce((result, path) => result && path && path.length > 0, true)
        ).reduce((result, path) => result && path, true);
    }

    /**
     * Check that you can return to a cell starting from it without going
     * back the same way you traveled and returns the shortest path of all.
     *
     * @returns {Uint8Array | number[]} A list of the shortest paths
     * traveled for each cell.
     */
    checkReturningPath() {
        let passableCells = this.cells.map(row => row.filter(cell => cell.canGoTrough));
        let passableCellsFlat = passableCells && [].concat.apply([], passableCells);
        return passableCellsFlat && passableCellsFlat.map(cell => cell.startTrip);
    }

    /**
     * Retrieves a list with the amount of connects of every cell in the maze.
     *
     * @returns {Uint8Array | number[]} A list of connections count, never null.
     */
    getCellsConnections() {
        let passableCells = this.cells.map(row => row.filter(cell => cell.canGoTrough));
        let passableCellsFlat = passableCells && [].concat.apply([], passableCells);
        return passableCellsFlat && passableCellsFlat.map(cell => cell.connectionsCount);
    }

    getCell(row, col) {
        return this.cells[row][col];
    }

    createGrid(cellMatrix, nonPassableTypes, cellImpassable) {
        let matrix = cellMatrix.map(row => row.map(cell => nonPassableTypes.includes(cell) ? 1 : 0));
        if (cellImpassable !== undefined) {
            matrix[cellImpassable.row][cellImpassable.col] = 1;
        }
        this.grid = new PF.Grid(matrix);
    }

    cloneThis( cellPrev ) {
        let grid = this.grid.clone();
        if ( cellPrev !== undefined ) {
            grid.nodes[cellPrev.row][cellPrev.col].walkable = false;
        }
        return grid;
    }

    getPath(cellStart, cellEnd, cellPrev) {
        let finder = new PF.AStarFinder();
        return finder.findPath(cellStart.col, cellStart.row, cellEnd.col, cellEnd.row, this.cloneThis(cellPrev));
    }

    getNextCellOnThePath(cellStart, cellEnd, cellPrev) {
        let path = this.getPath(cellStart, cellEnd, cellPrev);
        let next = path[0];
        if (path.length > 1) {
            next = path[1];
        }
        return this.getCell(next[1], next[0]);
    }
}

class Singleton {

    constructor(cellMatrix, nonPassableTypes) {
        if (!Singleton.instance) {
            Singleton.instance = new Maze(cellMatrix, nonPassableTypes);
        }
    }

    getInstance() {
        return Singleton.getInstance();
    }

    static getInstance() {
        return Singleton.instance;
    }

    static deleteInstance() {
        delete Singleton.instance;
    }

}

module.exports = Singleton;