const Cell = require("./Cell");
const Block = require("./Block");

/**
 * Factory of {@link MazeUnit mazeUnits}.
 */
class MazeUnitFactory {

    /**
     * Creates a {@link MazeUnit} based on the type of it. If the location
     * in the maze configuration represents a passable type a {@link Cell}
     * is return, otherwise the result is a {@link Block}.
     *
     * @param row The row in the maze matrix configuration.
     * @param col The column in the maze matrix configuration.
     * @param type The type of the cell.
     * @param nonPassableTypes A list of non passable types.
     * @returns {*} A cell if type is a passable type, otherwise a block.
     */
    static createMazeUnit(row, col, type, nonPassableTypes) {
        if (nonPassableTypes) {
            if (nonPassableTypes && nonPassableTypes.indexOf(type) < 0) {
                return new Cell(row, col);
            } else {
                return new Block(row, col);
            }
        } else {
            throw new Error("Non passable type array null.")
        }
    }
}

module.exports = MazeUnitFactory;