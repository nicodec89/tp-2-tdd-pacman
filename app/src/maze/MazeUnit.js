/**
 * Is a representation of a position in a maze with its connections if it
 * has any.
 */
class MazeUnit {

    /**
     * Constructor tha receives the coordinates of the position to create.
     *
     * @param row The row of the unit in the maze.
     * @param col The column of the unit in the maze.
     */
    constructor(row, col) {
        this.row = row;
        this.col = col;
        this.connections = [];
    }

    /**
     * Adds all the connections of this unit depending on whether it is
     * passable or no.
     *
     * @param cells The cells where to find the connections.
     */
    addConnections(cells) {
        if (this.canGoTrough) {
            this._addUp(cells);
            this._addDown(cells);
            this._addLeft(cells);
            this._addRight(cells);
        }
    }

    /**
     * Checks whether a unit is equals to some other.
     *
     * @param unit The unit to compare with this, might be null.
     * @returns {*|boolean} True if the given unit is equals to this.
     */
    isEquals(unit) {
        return unit && unit.row === this.row && unit.col === this.col;
    }

    /**
     * Determines if the cell is passable.
     */
    get canGoTrough() {}

    /**
     * Starts a trip from this cell around the maze to check if you can
     * return to it.
     *
     * @return The shortest path traveled.
     */
    get startTrip() {}

    /**
     * Retrieves the quantity of connections that this unit has.
     *
     * @return {number} The connections quantity, never null.
     */
    get connectionsCount() {
        return this.connections.length;
    }

    /**
     * Finds the target unit through all of this unit connections.
     *
     * @param target The target unit to search.
     */
    findCell(target) {}

    /**
     * Adds the connection of this unit with the upper unit.
     *
     * @param cells The matrix of units to look for the connections.
     * @private
     */
    _addUp(cells) {
        let cell = cells && cells[this.row - 1] && cells[this.row - 1][this.col];
        this._addConnection(cell);
    }

    /**
     * Adds the connection of this unit with unit on the left.
     *
     * @param cells The matrix of units to look for the connections.
     * @private
     */
    _addLeft(cells) {
        let cell = cells && cells[this.row] && cells[this.row][this.col - 1];
        this._addConnection(cell);
    }

    /**
     * Adds the connection of this unit with the below unit.
     *
     * @param cells The matrix of units to look for the connections.
     * @private
     */
    _addDown(cells) {
        let cell = cells && cells[this.row + 1] && cells[this.row + 1][this.col];
        this._addConnection(cell);
    }

    /**
     * Adds the connection of this unit with unit on the right.
     *
     * @param cells The matrix of units to look for the connections.
     * @private
     */
    _addRight(cells) {
        let cell = cells && cells[this.row] && cells[this.row][this.col + 1];
        this._addConnection(cell);
    }

    /**
     * Add's a connection of this unit with the given unit if this is a
     * passable unit.
     *
     * @param cell The unit to connect with, might be null.
     * @private
     */
    _addConnection(cell) {
        if (cell && cell.canGoTrough) {
            this.connections.push(cell);
        }
    }
}

module.exports = MazeUnit;