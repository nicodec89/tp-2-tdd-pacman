module.exports = {
    MINIMUM_CONNECTIONS: 'One of the cells has less than 2 connections.',
    FOUR_CELLS_EDGED: 'Four steps path found.',
    COULD_NOT_REACH_CELL: 'The maze is not totally connected.',
    NO_PASSABLE_CELLS: 'The maze does not have passable cells.',
    NON_EXISTENT_MAZE: 'No maze was found in the request body.'
};