const Intelligence = require('./Intelligence');
const DirectedMovement = require('../move/DirectedMovement');
const Position = require('./../move/Position');

class KeeperIntelligence extends Intelligence {

    constructor(ghost) {
        super(ghost);
        this._prevKeep = null;
    }

    moveAStep() {
        return this.directedMovement();
    }

    getPrevKeep() {
        return this._prevKeep;
    }

    setKeepPosition(value, prevKeep) {

        this._keepPosition = value;
        this._prevKeep = prevKeep;
    }

    directedMovement() {
        let movement = new DirectedMovement();

        //if come to destiny
        if ( Position.areEquals(this.ghost.getPosition(), this.getKeepPosition()) ) {
            this.turnPositions();
        }

        let moveIt = movement.moveAStep(this.ghost.getPosition(), this.ghost.getPreviousPosition(), this.getKeepPosition());

        return moveIt;
    }

    turnPositions() {
        let swap = this.getKeepPosition();
        this._keepPosition = this.getPrevKeep();
        this._prevKeep = swap;
    }
}

module.exports = KeeperIntelligence;