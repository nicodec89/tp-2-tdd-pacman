const Intelligence = require('./Intelligence');

class SearcherIntelligence extends Intelligence {

    moveAStep() {
        if ( this.ghost.getLastKnownPositionPacman() !== null ) {
            return this.directedMovement();
        } else {
            return this.randomMovement();
        }
    }
}

module.exports = SearcherIntelligence;