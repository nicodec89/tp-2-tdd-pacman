const RandomMovement = require('../move/RandomMovement');
const DirectedMovement = require('../move/DirectedMovement');

class Intelligence {

    constructor(ghost) {
        this.ghost = ghost;
        this._keepPosition = null;
    }

    moveAStep() {
        throw new Error('You have to implement the method move!');
    }

    randomMovement() {
        let movement = new RandomMovement();
        return movement.moveAStep(this.ghost.getPosition(), this.ghost.getPreviousPosition());
    }

    directedMovement() {
        let movement = new DirectedMovement();
        return movement.moveAStep(this.ghost.getPosition(), this.ghost.getPreviousPosition(), this.ghost.getLastKnownPositionPacman());
    }

    setKeepPosition(value) {
        this._keepPosition = null;
    }

    getKeepPosition(){
        return this._keepPosition;
    }
}

module.exports = Intelligence;