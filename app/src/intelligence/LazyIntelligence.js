const Intelligence = require('./Intelligence');

class LazyIntelligence extends Intelligence {

    moveAStep() {
        if (this.ghost.isWatchingPacman()) {
            return this.directedMovement();
        } else {
            return this.randomMovement();
        }
    }
}

module.exports = LazyIntelligence;