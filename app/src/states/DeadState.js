const StateInterface = require('./StateInterface');
/**
 * State when cannot hunting or be hunting
 */

const AMOUNT_DEAD = 2;

class DeadState extends StateInterface{

    constructor() {
        super();
        this._amountTurns = 0;
    }

    resetTurns() {
        this.amountTurns = super.resetTurns();
    }

    static get AMOUNT_DEAD(){
        return AMOUNT_DEAD;
    }

    set amountTurns( value ) {
        this._amountTurns = value;
    }

    get amountTurns() {
        return this._amountTurns;
    }

    plusToTurns() {
        this._amountTurns ++;
    }

    toRevive( ghost ){
        //si pasaron los turnos de muerto
        if ( this.amountTurns === DeadState.AMOUNT_DEAD ) {
            ghost.changeToHunter();
        }
    }

    moveAStep(ghost, cells) {
        //if was died dont move any step, add to turns
        this.plusToTurns();
        this.toRevive( ghost );
    }

    changeStatus( ghost ) {
        // the ghost is dead and nothing happen
    }

}

module.exports = DeadState;