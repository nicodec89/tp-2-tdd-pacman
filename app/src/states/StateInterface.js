const Position  = require('./../move/Position');
/**
 * Maze's element's states.
 */

const emptyTurns = 0;
// Anger levels
const V1 = 3;
const V2 = 6;
const V3 = 9;

class StateInterface {

    constructor( ) {
    }

    static get emptyTurns(){
        return emptyTurns;
    }

    static get V1(){
        return V1;
    }

    static get V2(){
        return V2;
    }

    static get V3(){
        return V3;
    }


    resetTurns() {
        return emptyTurns;
    }

    moveAStep( ghost, cells ) {

        let oldPos = ghost.getPosition().transformData();
        let newPos = ghost.intelligence.moveAStep();

        ghost.setPosition(newPos);
        ghost.setPreviousPosition(new Position(oldPos));
    }
}

module.exports = StateInterface;