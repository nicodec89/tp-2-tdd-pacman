const StateInterface = require('./StateInterface');
/**
 * Mode when element will be hunted
 */

const AMOUNT_PREY = 2;

class PreyState extends StateInterface {

    constructor() {
        super();
        this._amountTurns = 0;
    }

    static get AMOUNT_PREY(){
        return AMOUNT_PREY;
    }

    resetTurns() {
        this.amountTurns = super.resetTurns();
    }

    set amountTurns( value ) {
        this._amountTurns = value;
    }

    get amountTurns() {
        return this._amountTurns;
    }

    plusToTurns() {
        this._amountTurns ++;
    }

    noMorePrey( ghost ) {
        if ( this.amountTurns === PreyState.AMOUNT_PREY ) {
            ghost.changeToHunter();
        }
    }

    moveAStep(ghost, cells) {
        //falta ver que se aleje del pacman
        super.moveAStep(ghost, cells);
        this.plusToTurns();
        this.noMorePrey( ghost );
    }


    changeStatus( ghost ) {
        // the ghost was eaten
        ghost.changeToDead();
    }
}

module.exports = PreyState;