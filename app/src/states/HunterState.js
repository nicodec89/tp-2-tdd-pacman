const StateInterface = require('./StateInterface');
const HunterMediator = require('./HunterMediator');
/**
 * Mode when element is hunting
 */

class HunterState extends StateInterface {

    constructor( scopeVision ) {
        super();
        this._mediator = new HunterMediator(scopeVision);
        this._amountTurns = 0;
    }

    resetTurns() {
        this.amountTurns = super.resetTurns();
    }

    set amountTurns( value ) {
        this._amountTurns = value;
    }

    getAmountTurns() {
        return this._amountTurns;
    }

    getMediator() {
        return this._mediator;
    }

    plusToTurns() {
        this._amountTurns = this.getAmountTurns() + 1;
    }

    moveAStep(ghost, cells) {
        super.moveAStep(ghost, cells);
        this.plusToTurns();
        this.getMediator().calculateVision(this);
    }

    changeStatus( ghost ) {
        // eat the pacman, testing pass to prey
        ghost.changeToPrey();
    }

}

module.exports = HunterState;