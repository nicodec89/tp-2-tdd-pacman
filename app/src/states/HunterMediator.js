const StateInterface = require('./StateInterface');


const minVision = 1;
const medVision = 3;
const maxVision = 5;

class HunterMediator {

    constructor( scopeGhost ) {
        this._scopeGhost = scopeGhost;
    }

    static get minVision() {
        return minVision;
    }

    static get medVision() {
        return medVision;
    }

    static get maxVision() {
        return maxVision;
    }

    getScope() {
        return this._scopeGhost;
    }

    calculateVision( hunterState ) {

        if ( hunterState.getAmountTurns() === StateInterface.V1 ) {
            this.getScope().setMaxVision(HunterMediator.minVision);
        }

        if ( hunterState.getAmountTurns() === StateInterface.V2 ) {
            this.getScope().setMaxVision(HunterMediator.medVision);
        }

        if ( hunterState.getAmountTurns() === StateInterface.V3 ) {
            this.getScope().setMaxVision(HunterMediator.maxVision);
        }
    }

}

module.exports = HunterMediator;