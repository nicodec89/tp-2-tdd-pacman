const Subject = require('./Subject');
const Position = require('./../move/Position');

class Pacman extends Subject {

    static get COMPASS() {
        return {
            NORTH: {row: -1, col: 0},
            SOUTH: {row: 1, col: 0},
            WEST: {row: 0, col: -1},
            EAST: {row: 0, col: 1}
        };
    };

    /**
     * @param maze
     * @param position Initial position. Format: {position.row, position.col}
     * @param direction Direction in which it moves. Defined in COMPASS
     */
    constructor(maze, position, direction) {
        super();
        this._maze = maze;
        this._position = new Position(maze.getCell(position.row, position.col));
        this._direction = direction;
    }

    getPosition() {
        return this._position;
    }

    /**
     * This method notifies ghosts.
     */
    moveAStep() {
        let currentPosition = this.getPosition();
        let nextCell = this._maze.getCell(currentPosition.row + this._direction.row, currentPosition.col + this._direction.col);
        if (nextCell.canGoTrough) {
            this._position = new Position(nextCell);
        }
        this.notifyAll(this.getPosition());
    }

    changeDirection(direction) {
        this._direction = direction;
    }
}

module.exports = Pacman;