
class ScopeOfGhostsVision {
    constructor(maxDistanceVision, ghost) {
        this._maxDistanceVision = maxDistanceVision;
        this._ghost = ghost;
        this._lastKnownPositionPacman = null;
        this._isWatching = false;
    }

    updatePacmanPosition(positionPacman) {
        this._isWatching = false;
        if (this._ghost.getPosition().distance(positionPacman) <= this._maxDistanceVision) {
            this._lastKnownPositionPacman = positionPacman.clonePosition();
            this._isWatching = true;
        }
    }

    getMaxVision() {
        return this._maxDistanceVision;
    }

    isWatching () {
        return this._isWatching;
    }

    getLastKnownPositionPacman() {
        return this._lastKnownPositionPacman;
    }

    setMaxVision( value ) {
        this._maxDistanceVision = value;
    }

    clearLastKnownPositionPacman() {
        this._lastKnownPositionPacman = null;
        this._isWatching = false;
    }

}

module.exports = ScopeOfGhostsVision;