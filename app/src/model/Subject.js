
class Subject {
    constructor() {
        this.observers = [];
    }

    subscribe(observer) {
        this.observers.push(observer);
    }

    unsuscribe(observer) {
        let index = this.observers.indexOf(observer);
        if (index > -1) {
            this.observers.splice(index,1);

        }
    }

    notifyAll(data) {
        for (let o of this.observers) {
            o.update(data);
        }
    }
}

module.exports = Subject;