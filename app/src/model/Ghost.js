const Position = require('./../move/Position');
const ScopeOfGhostVision = require('./ScopeOfGhostsVision');
const HunterState = require('../states/HunterState');
const PreyState = require('../states/PreyState');
const DeadState = require('../states/DeadState');
const LazyIntelligence = require('../intelligence/LazyIntelligence');
const SearcherIntelligence = require('../intelligence/SearcherIntelligence');
const KeeperIntelligence = require('../intelligence/KeeperIntelligence');


/**
 * A phantom can be walked through cells.
 */
class Ghost {
    constructor(cell, ghostType) {
        this._previousPosition  = new Position(cell);
        this._position  = new Position(cell);
        this._scopeOfVision = new ScopeOfGhostVision(ghostType.rangeVision, this);
        this._state = new HunterState(this._scopeOfVision);
        this._intelligence = new ghostType.intelligence(this);
    }

    static get RANDOM() {
        return {
            TOP: {row: -1, col: 0},
            BOTTOM: {row: 1, col: 0},
            WEST: {row: 0, col: -1},
            EAST: {row: 0, col: 1}
        };
    };

    static get GHOST_TYPE() {
        return {
            SEARCHER: {rangeVision: 10, intelligence: SearcherIntelligence},
            TEMPERAMENTAL_SEARCHER: {rangeVision: 12, intelligence: SearcherIntelligence},
            LAZY: {rangeVision: 8, intelligence: LazyIntelligence},
            ZONZO: {rangeVision: 4, intelligence: LazyIntelligence},
            KEEPER: {rangeVision: 1000, intelligence: KeeperIntelligence}
        }
    }

    get movement() {
        return this._movement;
    }

    get intelligence() {
        return this._intelligence;
    }

    set movement(value) {
        this._movement = value;
    }

    setKeepPosition(value) {
        let keepPosition = Position.createAPositionWithoutConnections(value);
        this._intelligence.setKeepPosition(keepPosition, this.getPosition());
    }

    getPreviousPosition() {
        return this._previousPosition;
    }

    setPreviousPosition(value) {
        this._previousPosition = value;
    }

    getPosition() {
        return this._position;
    }

    setPosition(value) {
        this._position = value;
    }

    setState( value ) {
        this._state = value;
    }

    getState() {
        return this._state;
    }

    moveAStep( cells ) {
        this.crashWithPacman();
        this.getState().moveAStep( this, cells );
        this.crashWithPacman();
    }

    /**
     * States
     */

    changeToPrey() {
        this.setState(new PreyState());
    }

    changeToDead() {
        this.setState(new DeadState());
    }

    changeToHunter() {
        this.setState(new HunterState(this.getScopeOfVision()));
    }

    crashWithPacman() {

        let equal = false;
        if ( this.getLastKnownPositionPacman() !== null
            && this.getPosition() !== null
            && this.getPosition() !== undefined
            && this.getLastKnownPositionPacman() !== undefined) {
            equal = Position.areEquals(this.getPosition(), this.getLastKnownPositionPacman());
        }

        if ( equal ) {
            this.getState().changeStatus(this);
        }

        return equal;

    }

    /**
     * It is called by pacman when it move.
     */
    update(positionPacman) {
        this.updatePositionPacman(positionPacman);

        this.moveAStep();
    }

    updatePositionPacman(positionPacman) {
        this._scopeOfVision.updatePacmanPosition(positionPacman);
    }

    getLastKnownPositionPacman() {
        return this._scopeOfVision.getLastKnownPositionPacman();
    }

    isWatchingPacman() {
        return this._scopeOfVision.isWatching();
    }

    getScopeOfVision() {
        return this._scopeOfVision;
    }
}

module.exports = Ghost;