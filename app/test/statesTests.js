var expect = require("chai").expect;
var should = require('should');
var assert = require('assert');
let sinon = require('sinon');

let Pacman = require('../src/model/Pacman');
var Ghost = require('../src/model/Ghost');
var Maze = require('../src/maze/Maze.js');
var Position = require('../src/move/Position');
let HunterState = require('./../src/states/HunterState');
let DeadState = require('./../src/states/DeadState');
let PreyState = require('./../src/states/PreyState');

describe("States", function () {

    before(function () {
        // runs before all tests in this block
    });

    after(function () {
        // runs after all tests in this block
    });

    beforeEach(function () {
        // runs before each test in this block
    });

    afterEach(function () {
        // runs after each test in this block
    });

    describe('Creating ghost hunter', function () {
        it('should get hunter state.....', function () {

            let data = {
                "cells": [
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 1, 2, 1, 2, 1, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 1, 2, 1, 2, 1, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 0, 0, 0, 0, 0, 0]
                ],
                "impassableTypes": [
                    0,
                    2
                ]
            };

            let maze            = new Maze(data.cells, data.impassableTypes).getInstance();
            let passableCells   = maze.getPassableCells();
            let ghost_type = Ghost.GHOST_TYPE.ZONZO;
            ghost_type.rangeVision = 2;
            let ghost = new Ghost( passableCells[0], ghost_type);

            ghost.moveAStep(passableCells);

            let isHunter = (ghost.getState().constructor === HunterState);

            assert.equal(isHunter, true);
        });

        it('should get dead state.....', function () {

            let data = {
                "cells": [
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 1, 2, 1, 2, 1, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 1, 2, 1, 2, 1, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 0, 0, 0, 0, 0, 0]
                ],
                "impassableTypes": [
                    0,
                    2
                ]
            };

            let maze            = new Maze(data.cells, data.impassableTypes).getInstance();
            let passableCells   = maze.getPassableCells();

            let ghost_type = Ghost.GHOST_TYPE.ZONZO;
            ghost_type.rangeVision = 2;
            let ghost = new Ghost( passableCells[0], ghost_type);
            ghost.moveAStep(passableCells);
            ghost.changeToDead();

            let isDead = (ghost.getState().constructor === DeadState);

            assert.equal(isDead, true);
        });

        it('should get prey state.....', function () {

            let data = {
                "cells": [
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 1, 2, 1, 2, 1, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 1, 2, 1, 2, 1, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 0, 0, 0, 0, 0, 0]
                ],
                "impassableTypes": [
                    0,
                    2
                ]
            };

            let maze            = new Maze(data.cells, data.impassableTypes).getInstance();
            let passableCells   = maze.getPassableCells();

            let ghost_type = Ghost.GHOST_TYPE.ZONZO;
            ghost_type.rangeVision = 2;
            let ghost = new Ghost( passableCells[0], ghost_type);
            //let pacman = new Pacman(maze, {row: 1, col: 2}, Pacman.COMPASS.WEST);

            //pacman.subscribe(ghost);

            //pacman.moveAStep();
            ghost.moveAStep(passableCells);
            ghost.changeToPrey();

            let isPrey = (ghost.getState().constructor === PreyState);

            assert.equal(isPrey, true);
        });

        it('should get dead state when crash with pacman.....', function () {

            let data = {
                "cells": [
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 1, 2, 1, 2, 1, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 1, 2, 1, 2, 1, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 0, 0, 0, 0, 0, 0]
                ],
                "impassableTypes": [
                    0,
                    2
                ]
            };

            let maze            = new Maze(data.cells, data.impassableTypes).getInstance();
            let passableCells   = maze.getPassableCells();

            let ghost_type = Ghost.GHOST_TYPE.ZONZO;
            ghost_type.rangeVision = 2;
            let ghost = new Ghost( passableCells[0], ghost_type);
            let pacman = new Pacman(maze, {row: 1, col: 2}, Pacman.COMPASS.WEST);

            pacman.subscribe(ghost);
            pacman.moveAStep();

            ghost.changeToPrey();
            ghost.moveAStep(passableCells);

            while ( !Position.areEquals(ghost.getPosition(), pacman.getPosition()) ) {
                ghost.moveAStep(passableCells);
                //trick cause dont use directed move in this time
                ghost.getState().amountTurns = 0;
            }

            let isDead = (ghost.getState().constructor === DeadState);

            assert.equal( isDead, true);
        });

        it('should revive in two turns.....', function () {

            let data = {
                "cells": [
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 1, 2, 1, 2, 1, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 1, 2, 1, 2, 1, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 0, 0, 0, 0, 0, 0]
                ],
                "impassableTypes": [
                    0,
                    2
                ]
            };

            let maze            = new Maze(data.cells, data.impassableTypes).getInstance();
            let passableCells   = maze.getPassableCells();

            let ghost_type = Ghost.GHOST_TYPE.ZONZO;
            ghost_type.rangeVision = 2;
            let ghost = new Ghost( passableCells[0], ghost_type);

            ghost.moveAStep(passableCells);
            ghost.changeToDead();

            //only two turns
            for( let i = 0; i < 2; i++ ) {
                ghost.moveAStep(passableCells);
            }

            let isZombie = (ghost.getState().constructor === HunterState);

            assert.equal( isZombie, true);
        });

        it('should increase anger with every turn in hunter and provide more vision to ghost.....', function () {

            let data = {
                "cells": [
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 1, 2, 1, 2, 1, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 1, 2, 1, 2, 1, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 0, 0, 0, 0, 0, 0]
                ],
                "impassableTypes": [
                    0,
                    2
                ]
            };

            let maze            = new Maze(data.cells, data.impassableTypes).getInstance();
            let passableCells   = maze.getPassableCells();
            let ghost_type = Ghost.GHOST_TYPE.ZONZO;
            ghost_type.rangeVision = 2;
            let ghost = new Ghost( passableCells[0], ghost_type);

            ghost.moveAStep(passableCells);

            //define 9 to get max vision
            for ( let i = 0; i < 9; i++ ) {
                ghost.moveAStep(passableCells);
            }

            assert.equal( ghost.getScopeOfVision().getMaxVision(), 5);
        });

    });


});