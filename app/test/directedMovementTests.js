let expect = require("chai").expect;
let should = require('should');
let assert = require('assert');
let sinon = require('sinon');

let DirectedMovement = require('../src/move/DirectedMovement');
let Position = require('../src/move/Position');
let Maze = require('../src/maze/Maze');

describe("DirectedMovement", function () {

    before(function () {
        // runs before all tests in this block
    });

    after(function () {
        // runs after all tests in this block
    });

    beforeEach(function () {
        // runs before each test in this block
    });

    afterEach(function () {
        // runs after each test in this block
        Maze.deleteInstance();
    });

    describe('movement', function () {
        it('case 1, it moves until it reaches the pacman', function () {
            let cells = [
                [0,0,0,0,0,0],
                [0,1,1,1,1,0],
                [0,1,0,0,1,0],
                [0,1,1,1,1,0],
                [0,0,0,0,0,0]
            ];
            let impassableTypes= [0,2];
            let maze = new Maze(cells, impassableTypes).getInstance();
            let posGhost = new Position(maze.getCell(1,1));
            let prevPosGhost = new Position(maze.getCell(1,1));
            let posPacman = new Position(maze.getCell(1,4));

            let directedMovement = new DirectedMovement();

            let nextPos = directedMovement.moveAStep(posGhost, prevPosGhost, posPacman);
            assert.equal(nextPos.row, 1);
            assert.equal(nextPos.col, 2);

            posGhost = new Position(maze.getCell(1,2));
            prevPosGhost = new Position(maze.getCell(1,1));
            nextPos = directedMovement.moveAStep(posGhost, prevPosGhost, posPacman);
            assert.equal(nextPos.row, 1);
            assert.equal(nextPos.col, 3);

            posGhost = new Position(maze.getCell(1,3));
            prevPosGhost = new Position(maze.getCell(1,2));
            nextPos = directedMovement.moveAStep(posGhost, prevPosGhost, posPacman);
            assert.equal(nextPos.row, 1);
            assert.equal(nextPos.col, 4);
        });
        it('case 2, it moves until it reaches the pacman', function () {
            let cells = [
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0],
                [0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0],
                [0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0],
                [0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0],
                [0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            ];
            let impassableTypes= [0,2];
            let maze = new Maze(cells, impassableTypes).getInstance();
            let posGhost = new Position(maze.getCell(5,5));
            let prevPosGhost = new Position(maze.getCell(5,5));
            let posPacman = new Position(maze.getCell(1,1));

            let directedMovement = new DirectedMovement();

            let nextPos = directedMovement.moveAStep(posGhost, prevPosGhost, posPacman);
            assert.equal(nextPos.row, 4);
            assert.equal(nextPos.col, 5);

            posGhost = new Position(maze.getCell(4,5));
            prevPosGhost = new Position(maze.getCell(5,5));
            nextPos = directedMovement.moveAStep(posGhost, prevPosGhost, posPacman);
            assert.equal(nextPos.row, 3);
            assert.equal(nextPos.col, 5);

            posGhost = new Position(maze.getCell(3,5));
            prevPosGhost = new Position(maze.getCell(4,5));
            nextPos = directedMovement.moveAStep(posGhost, prevPosGhost, posPacman);
            assert.equal(nextPos.row, 2);
            assert.equal(nextPos.col, 5);

            posGhost = new Position(maze.getCell(2,5));
            prevPosGhost = new Position(maze.getCell(3,5));
            nextPos = directedMovement.moveAStep(posGhost, prevPosGhost, posPacman);
            assert.equal(nextPos.row, 2);
            assert.equal(nextPos.col, 4);

            posGhost = new Position(maze.getCell(2,4));
            prevPosGhost = new Position(maze.getCell(2,5));
            nextPos = directedMovement.moveAStep(posGhost, prevPosGhost, posPacman);
            assert.equal(nextPos.row, 2);
            assert.equal(nextPos.col, 3);

            posGhost = new Position(maze.getCell(2,3));
            prevPosGhost = new Position(maze.getCell(2,4));
            nextPos = directedMovement.moveAStep(posGhost, prevPosGhost, posPacman);
            assert.equal(nextPos.row, 1);
            assert.equal(nextPos.col, 3);

            posGhost = new Position(maze.getCell(1,3));
            prevPosGhost = new Position(maze.getCell(2,3));
            nextPos = directedMovement.moveAStep(posGhost, prevPosGhost, posPacman);
            assert.equal(nextPos.row, 1);
            assert.equal(nextPos.col, 2);

            posGhost = new Position(maze.getCell(1,2));
            prevPosGhost = new Position(maze.getCell(1,3));
            nextPos = directedMovement.moveAStep(posGhost, prevPosGhost, posPacman);
            assert.equal(nextPos.row, 1);
            assert.equal(nextPos.col, 1);
        });
    });
});