var expect = require("chai").expect;
var should = require('should');
var assert = require('assert');

var Ghost = require('../src/model/Ghost');
var Maze = require('../src/maze/Maze.js');
const Position = require('../src/move/Position');

describe("Ghost", function () {

    before(function () {
        // runs before all tests in this block
    });

    after(function () {
        // runs after all tests in this block
    });

    beforeEach(function () {
        // runs before each test in this block
    });

    afterEach(function () {
        // runs after each test in this block
        Maze.deleteInstance();
    });

    describe('Looking Walls', function () {

        it('should start in the first passable cell.....', function () {
            let data = {
                "cells": [
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 1, 2, 1, 2, 1, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 1, 2, 1, 2, 1, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 0, 0, 0, 0, 0, 0]
                ],
                "impassableTypes": [
                    0,
                    2
                ]
            };

            let maze            = new Maze(data.cells, data.impassableTypes).getInstance();
            let passableCells   = maze.getPassableCells();
            let ghost           = new Ghost(passableCells[0], Ghost.GHOST_TYPE.ZONZO);
            let initial         = new Position(passableCells[0]);

            let is_start = Position.areEquals(ghost.getPosition(), initial);

            assert.equal( is_start, true);
        });

        it('should moved the ghost.....', function () {
            let data = {
                "cells": [
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 1, 2, 1, 2, 1, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 1, 2, 1, 2, 1, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 0, 0, 0, 0, 0, 0]
                ],
                "impassableTypes": [
                    0,
                    2
                ]
            };

            let maze            = new Maze(data.cells, data.impassableTypes).getInstance();
            let passableCells   = maze.getPassableCells();
            let ghost           = new Ghost(passableCells[0], Ghost.GHOST_TYPE.ZONZO);

            //move a position
            ghost.moveAStep();

            let areEqual = Position.areEquals(ghost.getPosition(), ghost.getPreviousPosition());

            assert.equal( areEqual, false);
        });

        it('should be position and previous position are different after movements.....', function () {
            let data = {
                "cells": [
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 1, 2, 1, 2, 1, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 1, 2, 1, 2, 1, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 0, 0, 0, 0, 0, 0]
                ],
                "impassableTypes": [
                    0,
                    2
                ]
            };

            let maze            = new Maze(data.cells, data.impassableTypes).getInstance();
            let passableCells   = maze.getPassableCells();
            let ghost           = new Ghost(passableCells[0], Ghost.GHOST_TYPE.ZONZO);

            for ( let i = 0; i < 10; i++) {
                ghost.moveAStep();
            }

            let areEqual = Position.areEquals(ghost.getPosition(), ghost.getPreviousPosition());

            assert.equal( areEqual, false);
        });
    });

    describe('Scope of vision', function () {
        it('pacman out of sight', function () {
            let data = {
                "cells": [
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 1, 2, 1, 2, 1, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 1, 2, 1, 2, 1, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 0, 0, 0, 0, 0, 0]
                ],
                "impassableTypes": [
                    0,
                    2
                ]
            };

            let maze = new Maze(data.cells, data.impassableTypes).getInstance();
            let ghost = new Ghost(maze.getCell(1,1), Ghost.GHOST_TYPE.ZONZO);
            let positionPacman = new Position(maze.getCell(5,5));

            ghost.updatePositionPacman(positionPacman);

            assert.equal(ghost.isWatchingPacman(), false);
            assert.equal(ghost.getLastKnownPositionPacman(), null);
        });
        it('pacman within the scope of vision', function () {
            let data = {
                "cells": [
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 1, 2, 1, 2, 1, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 1, 2, 1, 2, 1, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 0, 0, 0, 0, 0, 0]
                ],
                "impassableTypes": [
                    0,
                    2
                ]
            };

            let maze = new Maze(data.cells, data.impassableTypes).getInstance();
            let ghost_type = Ghost.GHOST_TYPE.ZONZO;
            ghost_type.rangeVision = 100;
            let ghost = new Ghost(maze.getCell(1,1), ghost_type);
            let positionPacman = new Position(maze.getCell(5,5));

            ghost.updatePositionPacman(positionPacman);

            assert.equal(ghost.isWatchingPacman(), true);
            assert(Position.areEquals(ghost.getLastKnownPositionPacman(), positionPacman));
        });
        it('pacman begins within sight and comes out', function () {
            let data = {
                "cells": [
                    [0, 0, 0, 0, 0, 0, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 1, 2, 1, 2, 1, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 1, 2, 1, 2, 1, 0],
                    [0, 1, 1, 1, 1, 1, 0],
                    [0, 0, 0, 0, 0, 0, 0]
                ],
                "impassableTypes": [
                    0,
                    2
                ]
            };
            let maze = new Maze(data.cells, data.impassableTypes).getInstance();
            let ghost_type = Ghost.GHOST_TYPE.ZONZO;
            ghost_type.rangeVision = 3;
            let ghost = new Ghost(maze.getCell(1,1), ghost_type);
            let positionPacman = new Position(maze.getCell(2,3));

            ghost.updatePositionPacman(positionPacman);

            assert.equal(ghost.isWatchingPacman(), true);
            assert(Position.areEquals(ghost.getLastKnownPositionPacman(),positionPacman));

            let newPositionPacman = positionPacman.clonePosition();
            newPositionPacman.row = 2;
            newPositionPacman.col = 4;
            ghost.updatePositionPacman(newPositionPacman);

            assert.equal(ghost.isWatchingPacman(), false);
            assert(Position.areEquals(ghost.getLastKnownPositionPacman(),positionPacman));
        });
    });
});