let expect = require("chai").expect;
let should = require('should');
let assert = require('assert');
let sinon = require('sinon');

let ScopeOfGhostsVision = require('../src/model/ScopeOfGhostsVision');
let Ghost = require('../src/model/Ghost');
let Position = require('../src/move/Position');

describe("ScopeOfGhostsVision", function () {

    before(function () {
        // runs before all tests in this block
    });

    after(function () {
        // runs after all tests in this block
    });

    beforeEach(function () {
        // runs before each test in this block
    });

    afterEach(function () {
        // runs after each test in this block
    });

    describe('initial state', function () {
        it('isWatching must be false at startup', function () {
            let cell = {
                row: 1,
                col: 1,
                connections: []
            };
            let ghost_type = Ghost.GHOST_TYPE.ZONZO;
            ghost_type.rangeVision = 5;
            let ghost = new Ghost(cell, ghost_type);
            let scopeOfGhostsVision = ghost.getScopeOfVision();
            assert.equal(scopeOfGhostsVision.isWatching(), false);
        });
        it('getLastKnownPositionPacman must be null at startup', function () {
            let cell = {
                row: 1,
                col: 1,
                connections: []
            };
            let ghost_type = Ghost.GHOST_TYPE.ZONZO;
            ghost_type.rangeVision = 5;
            let ghost = new Ghost(cell, ghost_type);
            let scopeOfGhostsVision = new ScopeOfGhostsVision(5, ghost);
            assert.equal(scopeOfGhostsVision.getLastKnownPositionPacman(), null);
        });
    });

    describe('update pacman position', function () {
        it('case 1, pacman out of sight range', function () {
            let cellGhost = {
                row: 4,
                col: 5,
                connections: []
            };
            let cellPacman = {
                row: 1,
                col: 1,
                connections: []
            };
            let ghost_type = Ghost.GHOST_TYPE.ZONZO;
            ghost_type.rangeVision = 5;
            let ghost = new Ghost(cellGhost, ghost_type);
            let scopeOfGhostsVision = ghost.getScopeOfVision();
            let positionPacman = new Position(cellPacman);

            scopeOfGhostsVision.updatePacmanPosition(positionPacman);

            assert.equal(scopeOfGhostsVision.isWatching(), false);
            assert.equal(scopeOfGhostsVision.getLastKnownPositionPacman(), null);
        });
        it('case 2, pacman within the scope of vision', function () {
            let cellGhost = {
                row: 1,
                col: 3,
                connections: []
            };
            let cellPacman = {
                row: 1,
                col: 1,
                connections: []
            };
            let ghost_type = Ghost.GHOST_TYPE.ZONZO;
            ghost_type.rangeVision = 5;
            let ghost = new Ghost(cellGhost, ghost_type);
            let scopeOfGhostsVision = ghost.getScopeOfVision();
            let positionPacman = new Position(cellPacman);

            scopeOfGhostsVision.updatePacmanPosition(positionPacman);

            assert.equal(scopeOfGhostsVision.isWatching(), true);
            assert(Position.areEquals(scopeOfGhostsVision.getLastKnownPositionPacman(),positionPacman));
        });
        it('case 3, pacman at the edge of the vision scope', function () {
            let cellGhost = {
                row: 1,
                col: 6,
                connections: []
            };
            let cellPacman = {
                row: 1,
                col: 1,
                connections: []
            };
            let ghost_type = Ghost.GHOST_TYPE.ZONZO;
            ghost_type.rangeVision = 5;
            let ghost = new Ghost(cellGhost, ghost_type);
            let scopeOfGhostsVision = ghost.getScopeOfVision();
            let positionPacman = new Position(cellPacman);

            scopeOfGhostsVision.updatePacmanPosition(positionPacman);

            assert.equal(scopeOfGhostsVision.isWatching(), true);
            assert(Position.areEquals(scopeOfGhostsVision.getLastKnownPositionPacman(),positionPacman));
        });
        it('case 4, pacman starts out of sight and enters', function () {
            let cellGhost = {
                row: 4,
                col: 5,
                connections: []
            };
            let cellPacman = {
                row: 1,
                col: 1,
                connections: []
            };
            let ghost_type = Ghost.GHOST_TYPE.ZONZO;
            ghost_type.rangeVision = 5;
            let ghost = new Ghost(cellGhost, ghost_type);
            let scopeOfGhostsVision = ghost.getScopeOfVision();
            let positionPacman = new Position(cellPacman);

            scopeOfGhostsVision.updatePacmanPosition(positionPacman);

            assert.equal(scopeOfGhostsVision.isWatching(), false);
            assert.equal(scopeOfGhostsVision.getLastKnownPositionPacman(), null);

            positionPacman.row = 2;
            positionPacman.col = 3;
            scopeOfGhostsVision.updatePacmanPosition(positionPacman);

            assert.equal(scopeOfGhostsVision.isWatching(), true);
            assert(Position.areEquals(scopeOfGhostsVision.getLastKnownPositionPacman(),positionPacman));
        });
        it('case 5, pacman begins within sight and comes out', function () {
            let cellGhost = {
                row: 4,
                col: 5,
                connections: []
            };
            let cellPacman = {
                row: 3,
                col: 3,
                connections: []
            };
            let ghost_type = Ghost.GHOST_TYPE.ZONZO;
            ghost_type.rangeVision = 5;
            let ghost = new Ghost(cellGhost, ghost_type);
            let scopeOfGhostsVision = ghost.getScopeOfVision();
            let positionPacman = new Position(cellPacman);

            scopeOfGhostsVision.updatePacmanPosition(positionPacman);

            assert.equal(scopeOfGhostsVision.isWatching(), true);
            assert(Position.areEquals(scopeOfGhostsVision.getLastKnownPositionPacman(),positionPacman));

            let newPositionPacman = positionPacman.clonePosition();
            newPositionPacman.row = 1;
            newPositionPacman.col = 2;
            scopeOfGhostsVision.updatePacmanPosition(newPositionPacman);

            assert.equal(scopeOfGhostsVision.isWatching(), false);
            assert(Position.areEquals(scopeOfGhostsVision.getLastKnownPositionPacman(),positionPacman));
        });
        it('case 6, pacman begins at sight and the ghost walks away', function () {
            let cellGhost = {
                row: 4,
                col: 5,
                connections: []
            };
            let cellPacman = {
                row: 3,
                col: 3,
                connections: []
            };
            let ghost_type = Ghost.GHOST_TYPE.ZONZO;
            ghost_type.rangeVision = 5;
            let ghost = new Ghost(cellGhost, ghost_type);
            let scopeOfGhostsVision = ghost.getScopeOfVision();
            let positionPacman = new Position(cellPacman);

            scopeOfGhostsVision.updatePacmanPosition(positionPacman);

            assert.equal(scopeOfGhostsVision.isWatching(), true);
            assert(Position.areEquals(scopeOfGhostsVision.getLastKnownPositionPacman(),positionPacman));

            let positionGhost = ghost.getPosition();
            positionGhost.row = 6;
            positionGhost.col = 6;
            ghost.position = positionGhost;
            scopeOfGhostsVision.updatePacmanPosition(positionPacman);

            assert.equal(scopeOfGhostsVision.isWatching(), false);
            assert(Position.areEquals(scopeOfGhostsVision.getLastKnownPositionPacman(),positionPacman));
        });
    });
});