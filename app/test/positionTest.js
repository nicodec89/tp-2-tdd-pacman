var expect = require("chai").expect;
var should = require('should');
var assert = require('assert');

var Ghost = require('../src/model/Ghost');
var Maze = require('../src/maze/Maze.js');
var Position = require('../src/move/Position');

describe("Position", function () {

    before(function () {
        // runs before all tests in this block
    });

    after(function () {
        // runs after all tests in this block
    });

    beforeEach(function () {
        // runs before each test in this block
    });

    afterEach(function () {
        // runs after each test in this block
    });

    describe('Looking Position', function () {
        it('should get position Row.....', function () {
            let posToCreate = {
                row: 3,
                col: 5,
                connections: [ ]
            };
            let position = new Position(posToCreate);
            assert.equal(position.row, 3);
        });

        it('should get position Col.....', function () {
            let posToCreate = {
                row: 3,
                col: 5,
                connections: [ ]
            };
            let position = new Position(posToCreate);
            assert.equal(position.col, 5);
        });

        it('should be different positions.....', function () {
            let posToCreate = {
                row: 3,
                col: 5,
                connections: [ ]
            };

            let otherPosToCreate = {
                row: 4,
                col: 5,
                connections: [ ]
            };

            let position1 = new Position(posToCreate);
            let position2 = new Position(otherPosToCreate);
            let is_equal = Position.areEquals( position1, position2);
            assert.equal(is_equal, false);
        });

        it('should be equals positions.....', function () {
            let posToCreate = {
                row: 3,
                col: 5,
                connections: [ ]
            };

            let otherPosToCreate = {
                row: 3,
                col: 5,
                connections: [ ]
            };

            let position1 = new Position(posToCreate);
            let position2 = new Position(otherPosToCreate);
            let is_equal = Position.areEquals( position1, position2);
            
            assert.equal(is_equal, true);
        });
    });

    describe('Distance', function () {
        it('distance with itself should be 0', function () {
            let cell = {
                row: 1,
                col: 1,
                connections: [ ]
            };

            let position = new Position(cell);

            assert.equal(position.distance(position), 0);
        });

        it('distance, case 2', function () {
            let cell1 = {
                row: 1,
                col: 1,
                connections: [ ]
            };

            let cell2 = {
                row: 1,
                col: 4,
                connections: [ ]
            };

            let position1 = new Position(cell1);
            let position2 = new Position(cell2);

            assert.equal(position1.distance(position2), 3);
        });

        it('distance, case 3', function () {
            let cell1 = {
                row: 1,
                col: 1,
                connections: [ ]
            };

            let cell2 = {
                row: 4,
                col: 5,
                connections: [ ]
            };

            let position1 = new Position(cell1);
            let position2 = new Position(cell2);

            assert.equal(position1.distance(position2), 7);
        });

        it('should return different positions.....', function () {
            let posToCreate = {
                row: 3,
                col: 7,
                connections: [ ]
            };

            let otherPosToCreate = {
                row: 3,
                col: 5,
                connections: [ ]
            };

            let position1 = new Position(posToCreate);
            let position2 = new Position(otherPosToCreate);
            let pos_different = position1.getDifferentPosition( posToCreate, position2);
            let is_equal = (pos_different === position2);

            assert.equal(is_equal, false);
        });
    });
});