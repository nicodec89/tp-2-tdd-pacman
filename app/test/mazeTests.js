let expect = require("chai").expect;
let should = require('should');
let assert = require('assert');

let Maze = require('../src/maze/Maze');

describe("Maze", function () {

    before(function () {
        // runs before all tests in this block
    });

    after(function () {
        // runs after all tests in this block
    });

    beforeEach(function () {
        // runs before each test in this block
    });

    afterEach(function () {
        // runs after each test in this block
        Maze.deleteInstance();
    });

    describe('get path', function () {
        it('case 1', function () {
            let cells = [
                [0,0,0,0,0,0],
                [0,1,1,1,1,0],
                [0,1,0,0,1,0],
                [0,1,1,1,1,0],
                [0,0,0,0,0,0]
            ];
            let impassableTypes= [0,2];
            let maze = new Maze(cells, impassableTypes).getInstance();
            cellStart = maze.getCell(1,1);
            cellEnd = maze.getCell(2,4);

            let path = maze.getPath(cellStart, cellEnd);

            assert.equal(path[0][0], 1); //col
            assert.equal(path[0][1], 1); //row

            assert.equal(path[1][0], 2); //col
            assert.equal(path[1][1], 1); //row

            assert.equal(path[2][0], 3); //col
            assert.equal(path[2][1], 1); //row

            assert.equal(path[3][0], 4); //col
            assert.equal(path[3][1], 1); //row

            assert.equal(path[4][0], 4); //col
            assert.equal(path[4][1], 2); //row
        });
    });
    describe('get next cell on the path', function () {
        it('case 1', function () {
            let cells = [
                [0,0,0,0,0,0],
                [0,1,1,1,1,0],
                [0,1,0,0,1,0],
                [0,1,1,1,1,0],
                [0,0,0,0,0,0]
            ];
            let impassableTypes= [0,2];
            let maze = new Maze(cells, impassableTypes).getInstance();
            cellStart = maze.getCell(1,1);
            cellEnd = maze.getCell(2,4);

            let cell = maze.getNextCellOnThePath(cellStart, cellEnd);

            assert.equal(cell.col, 2); //col
            assert.equal(cell.row, 1); //row
        });
    });
});