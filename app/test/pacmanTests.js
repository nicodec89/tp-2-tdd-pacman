let expect = require("chai").expect;
let should = require('should');
let assert = require('assert');
let sinon = require('sinon');

let Pacman = require('../src/model/Pacman');
let Maze = require('../src/maze/Maze');

describe("Pacman", function () {

    before(function () {
        // runs before all tests in this block
    });

    after(function () {
        // runs after all tests in this block
    });

    beforeEach(function () {
        // runs before each test in this block
    });

    afterEach(function () {
        // runs after each test in this block
        Maze.deleteInstance();
    });

    describe('initial states', function () {
        it('the compass should have directions north, south, east and west', function () {
            assert.equal(Pacman.COMPASS.hasOwnProperty('NORTH'), true);
            assert.equal(Pacman.COMPASS.hasOwnProperty('SOUTH'), true);
            assert.equal(Pacman.COMPASS.hasOwnProperty('EAST'), true);
            assert.equal(Pacman.COMPASS.hasOwnProperty('WEST'), true);
        });
        it('the pacman must remain in the same position if it does not move', function () {
            let cells = [
                [0,0,0,0,0,0],
                [0,1,1,1,1,0],
                [0,1,0,0,1,0],
                [0,1,1,1,1,0],
                [0,0,0,0,0,0]
            ];
            let impassableTypes= [0,2];
            let maze = new Maze(cells, impassableTypes).getInstance();
            let pacman = new Pacman(maze, {row: 1, col: 1}, Pacman.COMPASS.EAST);

            assert.equal(pacman.getPosition().row, 1);
            assert.equal(pacman.getPosition().col, 1);
        });
    });
    describe('movement', function () {
        it('the pacman should move one position to the right if one step moves east', function () {
            let cells = [
                [0,0,0,0,0,0],
                [0,1,1,1,1,0],
                [0,1,0,0,1,0],
                [0,1,1,1,1,0],
                [0,0,0,0,0,0]
            ];
            let impassableTypes= [0,2];
            let maze = new Maze(cells, impassableTypes).getInstance();
            let pacman = new Pacman(maze, {row: 1, col: 1}, Pacman.COMPASS.EAST);

            pacman.moveAStep();

            assert.equal(pacman.getPosition().row, 1);
            assert.equal(pacman.getPosition().col, 2);
        });
        it('the pacman should move one position down if one step moves south', function () {
            let cells = [
                [0,0,0,0,0,0],
                [0,1,1,1,1,0],
                [0,1,0,0,1,0],
                [0,1,1,1,1,0],
                [0,0,0,0,0,0]
            ];
            let impassableTypes= [0,2];
            let maze = new Maze(cells, impassableTypes).getInstance();
            let pacman = new Pacman(maze, {row: 1, col: 1}, Pacman.COMPASS.SOUTH);

            pacman.moveAStep();

            assert.equal(pacman.getPosition().row, 2);
            assert.equal(pacman.getPosition().col, 1);
        });
        it('the pacman must remain in the same position if try to move to the west where the wall is', function () {
            let cells = [
                [0,0,0,0,0,0],
                [0,1,1,1,1,0],
                [0,1,0,0,1,0],
                [0,1,1,1,1,0],
                [0,0,0,0,0,0]
            ];
            let impassableTypes= [0,2];
            let maze = new Maze(cells, impassableTypes).getInstance();
            let pacman = new Pacman(maze, {row: 1, col: 1}, Pacman.COMPASS.WEST);

            pacman.moveAStep();

            assert.equal(pacman.getPosition().row, 1);
            assert.equal(pacman.getPosition().col, 1);
        });
        it('the pacman must remain in the same position if try to move to the north where the wall is', function () {
            let cells = [
                [0,0,0,0,0,0],
                [0,1,1,1,1,0],
                [0,1,0,0,1,0],
                [0,1,1,1,1,0],
                [0,0,0,0,0,0]
            ];
            let impassableTypes= [0,2];
            let maze = new Maze(cells, impassableTypes).getInstance();
            let pacman = new Pacman(maze, {row: 1, col: 1}, Pacman.COMPASS.NORTH);

            pacman.moveAStep();

            assert.equal(pacman.getPosition().row, 1);
            assert.equal(pacman.getPosition().col, 1);
        });
        it('the pacman moves 4 steps until it hits the wall', function () {
            let cells = [
                [0,0,0,0,0,0],
                [0,1,1,1,1,0],
                [0,1,0,0,1,0],
                [0,1,1,1,1,0],
                [0,0,0,0,0,0]
            ];
            let impassableTypes= [0,2];
            let maze = new Maze(cells, impassableTypes).getInstance();
            let pacman = new Pacman(maze, {row: 1, col: 1}, Pacman.COMPASS.EAST);

            pacman.moveAStep();
            pacman.moveAStep();
            pacman.moveAStep();
            pacman.moveAStep();

            assert.equal(pacman.getPosition().row, 1);
            assert.equal(pacman.getPosition().col, 4);
        });
        it('the pacman moves 4 steps until it hits the wall. Then change the direction to the south and moves 1 step', function () {
            let cells = [
                [0,0,0,0,0,0],
                [0,1,1,1,1,0],
                [0,1,0,0,1,0],
                [0,1,1,1,1,0],
                [0,0,0,0,0,0]
            ];
            let impassableTypes= [0,2];
            let maze = new Maze(cells, impassableTypes).getInstance();
            let pacman = new Pacman(maze, {row: 1, col: 1}, Pacman.COMPASS.EAST);

            pacman.moveAStep();
            pacman.moveAStep();
            pacman.moveAStep();
            pacman.moveAStep();
            pacman.changeDirection(Pacman.COMPASS.SOUTH);
            pacman.moveAStep();

            assert.equal(pacman.getPosition().row, 2);
            assert.equal(pacman.getPosition().col, 4);
        });
        it('the pacman takes a full turn and returns to the same position.', function () {
            let cells = [
                [0,0,0,0,0,0],
                [0,1,1,1,1,0],
                [0,1,0,0,1,0],
                [0,1,1,1,1,0],
                [0,0,0,0,0,0]
            ];
            let impassableTypes= [0,2];
            let maze = new Maze(cells, impassableTypes).getInstance();
            let pacman = new Pacman(maze, {row: 1, col: 1}, Pacman.COMPASS.EAST);

            for (let i=0; i < 3; i++) {
                pacman.moveAStep();
            }

            pacman.changeDirection(Pacman.COMPASS.SOUTH);
            for (let i=0; i < 2; i++) {
                pacman.moveAStep();
            }

            pacman.changeDirection(Pacman.COMPASS.WEST);
            for (let i=0; i < 3; i++) {
                pacman.moveAStep();
            }

            pacman.changeDirection(Pacman.COMPASS.NORTH);
            for (let i=0; i < 2; i++) {
                pacman.moveAStep();
            }

            assert.equal(pacman.getPosition().row, 1);
            assert.equal(pacman.getPosition().col, 1);
        });
        it('the pacman moves 2 steps. Then change the direction against the wall and move 1 step', function () {
            let cells = [
                [0,0,0,0,0,0],
                [0,1,1,1,1,0],
                [0,1,0,0,1,0],
                [0,1,1,1,1,0],
                [0,0,0,0,0,0]
            ];
            let impassableTypes= [0,2];
            let maze = new Maze(cells, impassableTypes).getInstance();
            let pacman = new Pacman(maze, {row: 1, col: 1}, Pacman.COMPASS.EAST);

            pacman.moveAStep();
            pacman.moveAStep();
            pacman.changeDirection(Pacman.COMPASS.SOUTH);
            pacman.moveAStep();

            assert.equal(pacman.getPosition().row, 1);
            assert.equal(pacman.getPosition().col, 3);
        });
        it('calls update in ghost for moveAStep success', function () {
            let cells = [
                [0,0,0,0,0,0],
                [0,1,1,1,1,0],
                [0,1,0,0,1,0],
                [0,1,1,1,1,0],
                [0,0,0,0,0,0]
            ];
            let impassableTypes= [0,2];
            let maze = new Maze(cells, impassableTypes).getInstance();
            let ghost = {update: function() {}};
            let pacman = new Pacman(maze, {row: 1, col: 1}, Pacman.COMPASS.EAST);

            sinon.spy(ghost, "update");

            pacman.subscribe(ghost);
            pacman.moveAStep();

            assert(ghost.update.calledOnce);
            ghost.update.restore();
        });
        it('calls update many ghosts for moveAStep success', function () {
            let cells = [
                [0,0,0,0,0,0],
                [0,1,1,1,1,0],
                [0,1,0,0,1,0],
                [0,1,1,1,1,0],
                [0,0,0,0,0,0]
            ];
            let impassableTypes= [0,2];
            let maze = new Maze(cells, impassableTypes).getInstance();
            let ghost1 = {update: function() {}};
            let ghost2 = {update: function() {}};
            let pacman = new Pacman(maze, {row: 1, col: 1}, Pacman.COMPASS.EAST);

            sinon.spy(ghost1, "update");
            sinon.spy(ghost2, "update");

            pacman.subscribe(ghost1);
            pacman.subscribe(ghost2);
            pacman.moveAStep();

            assert(ghost1.update.calledOnce);
            assert(ghost2.update.calledOnce);
            ghost1.update.restore();
            ghost2.update.restore();
        });

    });
});