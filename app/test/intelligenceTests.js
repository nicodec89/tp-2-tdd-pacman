var expect = require("chai").expect;
var should = require('should');
var assert = require('assert');

var Intelligence = require('../src/intelligence/Intelligence');

describe("Intelligence", function () {

    before(function () {
        // runs before all tests in this block
    });

    after(function () {
        // runs after all tests in this block
    });

    beforeEach(function () {
        // runs before each test in this block
    });

    afterEach(function () {
        // runs after each test in this block
    });

    describe('#move()', function () {

        it('should not be able to call move', function () {
            let intelligence = new Intelligence();

            assert.throws(intelligence.moveAStep, new Error('You have to implement the method move!'));
        });
    });
});